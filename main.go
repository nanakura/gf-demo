package main

import (
	_ "asydgiudemo/internal/packed"

	"github.com/gogf/gf/v2/os/gctx"

	"asydgiudemo/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.New())
}
