package response

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
)

type AppResponse struct {
	Ctx context.Context
}

func (app *AppResponse) Success(message string, data ...interface{}) {
	if len(data) > 0 {
		g.RequestFromCtx(app.Ctx).Response.WriteJson(g.Map{
			"code":    200,
			"message": message,
			"data":    data[0],
		})
	} else {
		g.RequestFromCtx(app.Ctx).Response.WriteJson(g.Map{
			"code":    200,
			"message": message,
		})
	}
}

func (app *AppResponse) Failure(code int, message string, data ...interface{}) {
	if len(data) > 0 {
		g.RequestFromCtx(app.Ctx).Response.WriteJson(g.Map{
			"code":    code,
			"message": message,
			"data":    data[0],
		})
	} else {
		g.RequestFromCtx(app.Ctx).Response.WriteJson(g.Map{
			"code":    code,
			"message": message,
		})
	}
}
