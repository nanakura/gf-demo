package controller

import (
	v1 "asydgiudemo/api/v1"
	"asydgiudemo/internal/common/response"
	"asydgiudemo/internal/model/entity"
	"asydgiudemo/internal/service"
	"context"
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
)

var (
	User = cUser{}
)

type cUser struct{}

func (c *cUser) Count(ctx context.Context, req *v1.UserCountReq) (res *v1.UserCountRes, err error) {
	app := response.AppResponse{
		Ctx: ctx,
	}
	if count, err := service.User.Count(ctx); err == nil {
		app.Success("获取成功", count)
		//g.RequestFromCtx(ctx).Response.Writeln(count)
	} else {
		app.Success("获取失败", fmt.Sprintf("error: %v", err))
		//g.RequestFromCtx(ctx).Response.Writeln(fmt.Sprintf("error: %v", err))
	}
	return
}

func (c *cUser) GetUserByUsernameAndPassword(ctx context.Context, req *v1.UserGetUsernamePasswordReq) (res *v1.CommonJsonRes, err error) {
	app := response.AppResponse{
		Ctx: ctx,
	}
	r := g.RequestFromCtx(ctx)
	if err = r.Parse(&req); err != nil {
		app.Failure(500, fmt.Sprintf("%v", err))
		return
	}
	user, err := service.User.GetUserByUserNamePassword(ctx, entity.TUser{
		Username: req.Username,
		Password: req.Password,
	})
	app.Success("获取成功", g.Map{
		"user": user,
	})
	return
}
