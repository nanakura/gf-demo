package controller

import (
	"asydgiudemo/internal/common/response"
	"context"

	"asydgiudemo/api/v1"
)

var (
	Hello = cHello{}
)

type cHello struct{}

func (c *cHello) Hello(ctx context.Context, req *v1.HelloReq) (res *v1.HelloRes, err error) {
	app := response.AppResponse{
		Ctx: ctx,
	}
	app.Success("获取成功", "Hello World!")
	return
}
