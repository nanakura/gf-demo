package controller

import (
	v1 "asydgiudemo/api/v1"
	"asydgiudemo/internal/common/response"
	"asydgiudemo/internal/service"
	"context"
	"github.com/gogf/gf/v2/frame/g"
)

type authController struct{}

var Auth = authController{}

func (c *authController) Login(ctx context.Context, req *v1.AuthLoginReq) (res *v1.CommonJsonRes, err error) {
	app := response.AppResponse{
		Ctx: ctx,
	}
	token, expire := service.Auth().LoginHandler(ctx)
	app.Success("获取成功", g.Map{
		"token":  token,
		"expire": expire,
	})
	return
}

func (c *authController) RefreshToken(ctx context.Context, req *v1.AuthRefreshTokenReq) (res *v1.CommonJsonRes, err error) {
	app := response.AppResponse{
		Ctx: ctx,
	}
	token, expire := service.Auth().RefreshHandler(ctx)
	app.Success("获取成功", g.Map{
		"token":  token,
		"expire": expire,
	})
	return
}

func (c *authController) Logout(ctx context.Context, req *v1.AuthLogoutReq) (res *v1.AuthLogoutRes, err error) {
	service.Auth().LogoutHandler(ctx)
	return
}
