package service

import (
	"asydgiudemo/internal/dao"
	"asydgiudemo/internal/model/entity"
	"context"
)

var (
	User = sUser{}
)

type sUser struct{}

func (s *sUser) Count(ctx context.Context) (count int, err error) {
	return dao.TUser.Ctx(ctx).Count()
}

func (s *sUser) GetPermissions() (sub, obj, act string, err error) {
	// TODO get data from db
	sub = "alice"
	obj = "data2"
	act = "read"
	return
}

func (s *sUser) GetUserByUserNamePassword(ctx context.Context, in entity.TUser) (res entity.TUser, err error) {
	err = dao.TUser.Ctx(ctx).Where("username=? and password=?", in.Username, in.Password).Scan(&res)
	return
}
