package router

import (
	"asydgiudemo/internal/controller"
	"asydgiudemo/internal/middleware"
	"github.com/gogf/gf/v2/net/ghttp"
)

func BindGroups(s *ghttp.Server) {
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.Middleware(
			ghttp.MiddlewareHandlerResponse,
			middleware.Cors,
		)

		// Register route handlers.
		group.Bind(
			controller.Auth,
		)

		group.Group("/u", func(userGroup *ghttp.RouterGroup) {
			userGroup.Middleware(middleware.Casbin)
			//userGroup.Middleware(middleware.Auth)
			userGroup.Bind(controller.User)
		})
		group.Bind(controller.Hello)
	})
}
