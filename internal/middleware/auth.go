package middleware

import (
	"asydgiudemo/internal/common/response"
	"asydgiudemo/internal/service"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gogf/gf/v2/net/ghttp"
	"net/http"
)

//const (
//	tokenPrefix = "Bearer "
//)

//func Auth(r *ghttp.Request) {
//	token := r.GetHeader("Authorization")
//	tokenCache := tokenPrefix + ""
//	if token == tokenCache {
//		r.Middleware.Next()
//	} else {
//		r.Response.WriteStatus(http.StatusForbidden)
//	}
//}

func Auth(r *ghttp.Request) {
	service.Auth().MiddlewareFunc()(r)
	r.Middleware.Next()
}

func Casbin(r *ghttp.Request) {
	casbinHandler(r)
	r.Middleware.Next()
}

func casbinHandler(r *ghttp.Request) {
	ctx := r.GetCtx()
	app := response.AppResponse{
		Ctx: ctx,
	}

	// 获取enforcer
	e, err := service.CasbinEnforcer(ctx)
	if err != nil {
		app.Failure(http.StatusInternalServerError, fmt.Sprintf("error: %v", err))
		r.ExitAll()
		return
	}
	// 1. 从登录用户拿到角色
	sub, obj, act, err := service.User.GetPermissions()
	if err != nil {
		app.Failure(http.StatusInternalServerError, fmt.Sprintf("error: %v", err))
		r.ExitAll()
		return
	}
	// 2. casbin角色校验
	ok, err2 := e.Enforce(sub, obj, act)
	// 3. 校验通过则放行，否则返回403
	if err2 != nil {
		app.Failure(http.StatusInternalServerError, fmt.Sprintf("error: %v", err2))
		r.ExitAll()
		return
	}
	if !ok {
		app.Failure(http.StatusForbidden, "权限不足")
		r.ExitAll()
	}
}
