module asydgiudemo

go 1.15

require (
	github.com/casbin/casbin/v2 v2.47.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gogf/gf-jwt/v2 v2.0.1
	github.com/gogf/gf/v2 v2.0.4
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
