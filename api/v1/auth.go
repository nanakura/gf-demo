package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type AuthLoginReq struct {
	g.Meta `path:"/login" method:"post"`
}

type AuthRefreshTokenReq struct {
	g.Meta `path:"/refresh_token" method:"post"`
}

type AuthLogoutReq struct {
	g.Meta `path:"/logout" method:"post"`
}

type AuthLogoutRes struct {
}
