package v1

import "github.com/gogf/gf/v2/frame/g"

type UserCountReq struct {
	g.Meta `path:"/count" tags:"user" method:"get" summary:"用户相关api"`
}

type UserCountRes struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
}

type UserGetUsernamePasswordReq struct {
	g.Meta   `path:"/user" tags:"user" method:"get" summary:"根据用户名和密码获取用户"`
	Username string `p:"username"`
	Password string `p:"password"`
}
